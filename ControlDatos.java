package src;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observable;

public class ControlDatos {

    public static  void main(String[] args){
        System.out.println("\n-------CONTROL DE FLUJO DE DATOS------\n");
        System.out.println("Valores de Flujo de Datos\n");

        Integer[] array = {2,5,6,8,10,35,2,10};
        Integer[] Sumatoria = {2+5+6+8+10+35+2+10};

        Observable.fromArray(Sumatoria)
                .filter(promedio -> promedio == promedio )
                .subscribe(promedio -> System.out.println("(a) El promedio de Flujo de Datos es : "+ promedio/8));
        System.out.println("");


        Observable.fromArray(array)
                .filter(Array -> Array >= 10 )
                .subscribe(Array -> System.out.println("(b) Filtrado de Valores mayores o iguales a 10 son: " + Array));


        Observable.fromArray(Sumatoria)
                .filter(suma -> suma == suma )
                .subscribe(suma -> System.out.println("\n(c) Sumatoria del Flujo de Datos es  : "+ suma));





    }
}

