package src;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Observable;

public class PromedioProductos {

    public static  void main(String[] args){
        System.out.println("\n-------PRECIO Y NOMBRE DE PRODUCTOS------\n");
        System.out.println("* Vehículo_simple = 300\n* Vehículo_simple_aut = 300\n* Vehículo_doblet_traccion = 200\n* vehículo_alta_gama = 800\n* Motocicleta= 230\n");

        Integer[] numbers = {300,200,300,800,230};
        Integer[] sumas = {300+200+300+800+230};
        Integer[] Promedio = {300+200+300+800+230};
        Integer[] Letras = {300+800+230+200};

        Observable.fromArray(Promedio)
                .filter(promedio -> promedio >= promedio )
                .subscribe(promedio -> System.out.println("PROMEDIO GENERAL DE PRODUCTOS ES : "+ promedio/5));


        Observable.fromArray(sumas)
                .filter(suma -> suma >= suma )
                .subscribe(suma -> System.out.println("(a) La sumatoria de los productos son : "+ suma));

        Observable.fromArray(numbers)
                .filter(number -> number >= 800 )
                .subscribe(number -> System.out.println("(b) El valor maximo del listado de producto es: "+ number));


        Observable.fromArray(Letras)
                .filter(letra -> letra >= letra )
                .subscribe(letra -> System.out.println("(c) Sumatoria de Productos con letra a son : "+ letra));





    }
}
